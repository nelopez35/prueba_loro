  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.countdown.min.js"></script>
  <script src="js/wow.js"></script>
  <script src="js/custom.js"></script>
  <script src="contactform/contactform.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.js"></script>

<!-- End custom js for this page-->

<script>
  $( document ).ready(function() {
    $('#departament').on('change', function(){
      $('#city').empty();
        $.ajax({
            url: `/getcities/${this.value}/`,
            success: data => {
                data.cities.forEach(city =>
                    $('#city').append(`<option value="${city.id}">${city.name}</option>`)
                )
            }
        })
    })
  });
    
</script>