<!DOCTYPE html>
<html lang="en">

<head>
    @include('layout.partials.head')
</head>

<body>
  @yield('content')
  <footer class="footer">
    @include('layout.partials.footer')
  </footer>
  @include('layout.partials.footer-scripts')

</body>

</html>
