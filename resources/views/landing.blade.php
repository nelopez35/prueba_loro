@extends('layout')

@section('content')
<div class="content">
    <div class="container wow fadeInUp delay-03s">
      @if($winner)
      <div class="text-center native-winner">
        <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp delay-04s">
          <div class="img">
            <i class="fa fa-eye"></i>
          </div>
          <h3 class="abt-hd">Nuestro Ganador!</h3>
            <p>Felicidades a nuestro ganador: <h3 id="winner-name">{{$winner->name}} {{$winner->lastname}}</h3></p>
            <p><h3 id="winner-email">{{$winner->email}}</h3></p>

        </div>
      </div>
      @endif
      <div class="text-center winner" style="display:none">
        <div class="col-md-12 col-sm-12 col-xs-12 fadeInUp">
          <div class="img">
            <i class="fa fa-eye"></i>
          </div>
          <h3 class="abt-hd">Nuestro Ganador!</h3>
            <p>Felicidades a nuestro ganador: <h3 id="winner-name"></h3></p>
            <p><h3 id="winner-email"></h3></p>

        </div>
      </div>
      <div class="row">
        <div class="logo text-center">
          <h2>Inscribete para ser uno de los ganadores!!</h2>
        </div>
        
        <h2 class="subs-title text-center">Registra tus datos aqui!!!</h2>
        <div id="note"></div>
        <div id="sendmessage">Your message has been sent. Thank you!</div>
        <div id="errormessage"></div>
        <div class="subcription-info text-center">
          <form action="{{url('createContestants')}}" method="post" class="contactForm">
            <div class="form-group">
              <input type="text" name="name" class="form-control" id="name" placeholder="Tu nombre" data-rule="minlen:4@required" data-msg="Ingresa minimo 4 caracteres" />
              <div class="validation"></div>
            </div>
            <div class="form-group">
              <input type="text" name="lastname" class="form-control" id="lastname" placeholder="Tu Apellido" data-rule="minlen:4@required" data-msg="Ingresa minimo 4 caracteres" />
              <div class="validation"></div>
            </div>
            <div class="form-group">
              <input type="text" name="email" class="form-control" id="email" placeholder="Tu correo electronico" data-rule="minlen:4@email" data-msg="Ingresa un correo electronico valido" />
              <div class="validation"></div>
            </div>
            <div class="form-group">
              <input type="number" name="identification" class="form-control" id="identification" placeholder="Tu CC" data-rule="minlen:4@required" data-msg="Ingresa un numero de minimo 4 caracteres" />
              <div class="validation"></div>
            </div>
            <div class="form-group">
            	<select class="form-control " name="departament" id ="departament"  data-rule="required"  data-msg="Ingresa un numero de minimo 4 caracteres">
              <option>Seleccione un departamento</option>
              @foreach($departaments as $departament)
                  <option value="{{ $departament->id }}">{{ $departament->name }}</option>
              @endforeach
              
              </select>
              <div class="validation"></div>
            </div>
            <div class="form-group">
                    <select class="form-control " name="city" id ="city" data-rule="required">
                  <option>Seleccione una ciudad</option>
              </select>
              <div class="validation"></div>
            </div>
            <div class="form-group">
              <input type="number" class="form-control" name="phone" id="phone" placeholder="Telefono" data-rule="required" data-msg="Please enter at least 8 chars of subject" />
              <div class="validation"></div>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="checkbox" value="" id="habeasdata" data-rule="checked" data-msg="Acepta para continuar"  required>
              <label class="form-check-label" for="habeasdata">
                Acepto tratamiento de mis datos.
              </label>
              <div class="validation"></div>
            </div>
            {{ csrf_field() }}
            
            <div class="text-center"><button type="submit" class="contact-submit">Enviar</button></div>
          </form>
          <p class="sub-p">Prometemos no hacerte spam.</p>
        </div>
      </div>
    </div>
    
    <section id="about" class="section-padding">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 text-center">
            <div class="about-title">
              <h2>Acerca de nosotros</h2>
              <p>Desarrollador de software</p>
            </div>
          
          </div>
        </div>
      </div>
    </section>
  </div>

@endsection