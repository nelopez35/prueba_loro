<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Usuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('contestants')) {
            Schema::create('contestants', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name');
                $table->string('lastname');
                $table->string('email')->unique();
                $table->bigInteger('identification');
                $table->integer('city_id');
                $table->bigInteger('phone');
                $table->boolean('habeasdata');
                $table->boolean('winner')->default(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contestants');
    }
}
