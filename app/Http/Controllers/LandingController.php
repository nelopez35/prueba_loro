<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Departaments;
use App\Cities;
use App\Contestants;
use App\Events\Winner;

class LandingController extends Controller
{
    
    public function index(){

        $departaments =  Departaments::all();

        $winnerContestant =  Contestants::where('winner', 1)->first() ?? false;

        //event(new Winner());

        return view('landing')->with('departaments', $departaments)->with('winner',$winnerContestant);
    }

    public function getCities(Request $request, $id){
        if ($request->ajax()) {
            return response()->json([
                'cities' => Cities::where('departament_id', $id)->get()
            ]);
        }
    }
}
