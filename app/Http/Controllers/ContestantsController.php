<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Contestants;
use App\Events\Winner;


class ContestantsController extends Controller
{
    public function store(Request $request){
        
        try{
            $validator = Validator::make($request->all(), [
                'name'              => 'required|regex:/^[\pL\s\-]+$/u',
                'lastname'          => 'required|regex:/^[\pL\s\-]+$/u',
                'email'             => 'required|email|unique:contestants,email',
                'identification'    => 'required|numeric|min:6',
                'departament'       => 'required|exists:departaments,id',
                'city'              => 'required|exists:cities,id',
                'phone'             => 'required|numeric:min:9'
            ]);
            
            if ($validator->fails()) {

                $errors = '';

                foreach($validator->messages()->get('*') as $error){

                    $errors .= $error[0];
                }

                return response()->json(['status' => 'fail', 'msg' => $errors, 'token' => csrf_token()]);
            }
    
            $contestant = new Contestants;
            $contestant->name = $request->name;
            $contestant->lastname = $request->lastname;
            $contestant->email = $request->email;
            $contestant->identification = $request->identification;
            $contestant->city_id = $request->city;
            $contestant->phone = $request->phone;
            $contestant->habeasdata = 1;
            $contestant->save();

            event(new Winner());

            $winnerContestants =  Contestants::where('winner', 1)->count();

            return response()->json(['status' => 'ok', 'msg' => 'Fuiste registrado de forma existosa!', 'token' => csrf_token(), 'hasWinner' => $winnerContestants]);


        }catch(Exception $e){

            return response()->json(['status' => 'fail', 'msg' => 'Ocurrio un error en la operacion!', 'token' => csrf_token()]);
            
        }
        

    }

    public function getWinner(Request $request){
        if ($request->ajax()) {
            return response()->json([
                'winner' => Contestants::where('winner', 1)->first(), 'status' => 'ok'
            ]);
        }
    }
}
