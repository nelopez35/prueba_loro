<?php

namespace App\Listeners;

use App\Events\Winner;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Contestants;

class chooseWinner
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Winner  $event
     * @return void
     */
    public function handle(Winner $event)
    {
        $contestants = Contestants::all()->count();
        $winnerContestants =  Contestants::where('winner', 1)->count();
        if($contestants >= 5 && $winnerContestants == 0){
            
            $contestant = Contestants::orderByRaw("RAND()")->first();
            $contestant->winner = true;
            $contestant->save();

        }
    }
}
