<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Contestants;

class Contestants extends Model
{
    protected $table = 'contestants';
}
