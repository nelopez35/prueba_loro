<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departaments extends Model
{
    
    protected $table = 'departaments';
    
    public function cities()
    {
        return $this->hasMany('App\Cities');
    }
}


